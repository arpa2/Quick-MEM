/* This file is part of Quick MEM,
 *     https://gitlab.com/arpa2/Quick-MEM
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2022 Adriaan de Groot <groot@kde.org>
 */

#include "arpa2/quick-mem.h"

#include <errno.h>
#include <stdlib.h>

#ifdef DEBUG
#include <arpa2/except.h>
#else
#define log_debug(fmt,...)
#endif

typedef struct { int c; } context;

void demo_simple_pool() {
	context *c = NULL;
	/* Needs a cast because context** is not void** */
	bool r = mem_open(sizeof(context), (void **)&c);
	if (!r) {
		log_debug("Could not open pool, %s", strerror(errno));
		exit(1);
	}
	if (c == NULL) {
		log_debug("Context pointer is NULL.");
		exit(1);
	}
	c->c = 1;

	/* Allocate many small chunks, total about 16kbytes */
	for (mem_size_t i = 0; i < 128; ++i) {
		void *chunk = NULL;
		bool r = mem_alloc(c, i, &chunk);
		if (!r) {
			log_debug("Could not allocated %ld bytes, %s", i, strerror(errno));
			exit(1);
		}
		if (chunk == NULL) {
			log_debug("Chunk pointer for size %ld is NULL.", i);
			exit(1);
		}
	}
	/* Needs a cast because context** is not void** */
	mem_close((void **)&c);
}


int main (int argc, char **argv) {
	demo_simple_pool();
	return 0;
}
