/** @defgroup quickmem Quick MEM pooled memory management
 * @{
 * This is a generic library for memory pooling.
 *
 * Memory pools can help you write cleaner, more secure code:
 *  - pools simplify referential integrity
 *  - pools can help to avoid memory leakage
 *  - memory is delivered in zeroed state
 *  - memory is zeroed before freeing it
 *  - references must be NULL to allocate to them
 *  - references are set to NULL when they are freed
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */

/* This file is part of Quick MEM, version 0.1 and later.
 *     https://gitlab.com/arpa2/Quick-MEM
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */

#ifndef QUICK_MEM_H
#define QUICK_MEM_H


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>


/** @brief Memory buffer, represented as a pointer and a length.
 */
typedef struct membuf {
	uint8_t *bufptr;
	size_t   buflen;
} membuf;



/** @brief Integer representation of memory sizes.
 *
 * The largest size
 * to be allocated cannot exceed this integer type's value.
 * You can override to any unsigned integer type you like.
 * The default value is size_t, as for standard malloc().
 */
#ifndef MEM_SIZE_T
#define MEM_SIZE_T size_t
#endif
typedef MEM_SIZE_T mem_size_t;

/** @defgroup pool Pool Management
 * @{
 * The basic structure Quick MEM works on is the memory pool.
 * A memory pool manages a chunk of memory, and memory can be
 * allocated cheaply from the pool. Pools are returned to the
 * system, or recycled, as a whole, simplifying memory management.
 *
 * In Quick MEM, pools are associated with a "context", which can
 * be an application-defined data structure (of size zero, if need be).
 *
 * The functions for pool management generally return a boolean value,
 * true on success. When the functions fail, they return false, and
 * set `errno` to a somewhat-descriptive value.
 * - if no memory is available, or allocation failed, `ENOMEM`
 * - if the preconditions of the function are not satisfied
 *   (generally, if pointers are not NULL beforehand), `EINVAL`
 */


/** @brief Allocate a new memory pool.
 *
 * The new memory pool is identified by an application
 * context which owns the pool. When the context is cleaned up
 * (e.g. freed) then the pool is cleaned up as well,
 * through a call to mem_close().
 *
 * The idea behind allocating the application context
 * dynamically is that most applications (in ARPA2,
 * things like Quick DER for processsing DER packets)
 * have a context structure **anyway** which needs to be
 * passed around, and the pool can be attached invisibly
 * to the context. There is no need to reserve any space
 * in the context structure for Quick MEM's use.
 *
 * The context pointer is passed to other Quick MEM functions
 * like `mem_alloc()` and `mem_close()`.
 *
 * If the application does not have meaningful data,
 * pass a size of 0 (zero); the context pointer will
 * be allocated and the pool associated with that zero-sized context.
 *
 * When calling this function `*context` **must** be NULL
 * beforehand. On successful return, it is a pointer
 * to (zeroed-out) memory suitable to store the application's
 * context data in.
 *
 * This function fails fatally if `*context` is not NULL.
 *
 * This function returns true on success (and sets `*context`), false on failure.
 * When this function fails, it sets errno to ENOMEM.
 */
bool mem_open (mem_size_t context_size, void **context);


/** @brief Close a memory pool.
 *
 * This will free up any application data
 * after first clearing it to zero bytes.  This is the
 * opposite function to `mem_open()`.
 *
 * The pool does not have to be linked explicitly from the
 * context; the pool is attached invisibly to the context.
 *
 * This function requires a pointer to the context, that it
 * will set to NULL upon return.  If it is NULL already, no
 * cleanup is done.
 *
 * Any resources allocated with `mem_resource()` will be
 * zeroed and freed, but first their free_cb() is called
 * with a pointer to the context and block pointers.
 *
 * This function does not fail.
 */
void mem_close (void **context);


/** @brief Clear a memory pool, by zeroing and freeing all its allocations.
 *
 * Pass in the context pointer set by a previous call to `mem_open()`.
 * Passing in NULL (e.g. after already calling `mem_close()`) triggers
 * an assert() in debug builds, while in release builds it is acceptable
 * and does nothing.
 *
 * Blocks allocated for large requirements are usually
 * custom-sized, so they are freed; blocks with default
 * sizes are simply made empty.  Effectively, the default-
 * sized blocks are recycled.
 *
 * Callbacks installed in the memory pool are called
 * before the corresponding resources are freed.
 *
 * You can indicate how much memory to keep at most.
 * This is a multiple of the internal, unknowable, block size,
 * so set it to a number of bytes that you would like to keep.
 * Use MAX_INT, or bit-complement 0 (`~0`) to keep everything.
 *
 * This function does not fail.
 */
void mem_recycle (mem_size_t keep_at_most, void *context);


/** @brief Allocate memory in the memory pool.
 *
 * The given application context structure serves as the
 * identifier for the memory pool.  The context must have
 * been created with `mem_open()`. Allocated memory
 * will not be freed until `mem_close()` frees the entire
 * memory pool.
 *
 * The pointer to the allocated memory is returned through
 * `*block*`. Allocated memory is cleared with zero bytes.
 * When calling this function, `*block` must be NULL.
 *
 * This function returns true on success, or false on failure.
 * In case of failure, it will have set errno to ENOMEM.
 */
bool mem_alloc (void *context, mem_size_t block_size, void **block);


/** @brief Allocate memory with cleanup callback.
 *
 * Allocate memory to the memory pool as with `mem_alloc()`,
 * but setup a callback function to be called with the
 * memory during `mem_close()`.
 *
 * Resources will be freed in the reverse order of their
 * allocation with `mem_resource()`.
 *
 * The callback function `free_cb` must not be a NULL pointer.
 * The same preconditions and return values as `mem_alloc()` apply.
 */
bool mem_resource (void *context, mem_size_t block_size, void **block,
			void (*free_cb) (void *context, void *block));

/** @} */

/** @defgroup buffer Buffer Management
 * @{
 * A Quick MEM pool also supports dynamically resizable buffers.
 * These buffers are allocated from the pool and their lifetime is
 * tied to the pool. They behave like normal allocations from the
 * pool in all other respects.
 *
 * As a special case, a buffer -- from whatever source, including other
 * pools -- can be duplicated **into** a memory pool and treated as a
 * NUL-terminated C string with `mem_strdup()`.
 */

/** @brief Duplicate a character buffer.
 *
 * Duplicate a character string provided as a membuf,
 * so without trailing NUL character ('`\0`'), and make it
 * available in the pool associated to the context **with** a trailing NUL.
 *
 * Before this call, `*out_strbuf` must be NULL.
 *
 * Returns true on success, or false on failure. On success,
 * `*out_strbuf` points to the duplicated string in the pool.
 */
bool mem_strdup (void *context, membuf str, char **out_strbuf);


/** @brief Allocate a dynamically-sizeable memory buffer.
 *
 * Allocate a buffer of a given minimum size.
 *
 * This works by allocating the remainder of a memory
 * chunk for you, and this remaining space is returned
 * to you for continued use.  When closing buffers,
 * their unused portion is returned to the pool.
 *
 * The buffer should start with `buffer->bufptr` set to NULL,
 * and will be set to a bufptr not NULL upon success,
 * with buflen the actual buffer size.  You can use
 * all of the buffer, though you don't have to.  Use
 * the same buffer in _close and _resize calls.
 *
 * Returns true on success, or false on failure. On success,
 * `buffer->bufptr` points to memory in the pool.
 *
 * Applications can request a buffer of some minimum size
 * and then fill it with data. Optionally, the buffer can be
 * "capped off", returning any remaining bytes to the pool.
 * If the buffer needs to be larger, use `mem_buffer_resize()`
 * to obtain more memory in the pool.
 *
 * You can use this to gradually develop a memory space of variable
 * size, such as a string buffer or, very common, a DER
 * SEQUENCE OF that may not include all elements that
 * you can predict as a worst case.
 *
 * Often, you will not use any other memory during
 * the fillup of the buffer and all that happens is
 * a lowering of the allocated buffer.  But it is
 * not an error to allocate more memory, including
 * calling `mem_buffer_open()` for another buffer -- you will just force
 * the allocation of fresh pool blocks if you do so.
 *
 */
bool mem_buffer_open (void *context, mem_size_t min_size, membuf *buffer);


/** @brief Cap the size of a dynamically-sized memory buffer.
 *
 * Cap off a buffer to the desired size, and return the
 * remaining bytes to the chunk of the pool.  This is
 * only possible for memory allocated with `mem_buffer_open()`
 * and it can only be done once.  It is not a problem
 * however, to use other mem_ operations at the same
 * time, including other operations on membuf structures that
 * have not had `mem_buffer_close()` called on them.
 *
 * The freed memory will be zeroed, so having touched
 * it is not a problem.
 *
 * After closing a buffer, you cannot resize it anymore.
 * The buflen in the buffer will be updated nonetheless.
 *
 * This operation does not fail.
 */
void mem_buffer_close (void *context, mem_size_t final_size, membuf *buffer);


/** @brief Delete a dynamically-sized buffer.
 *
 * Thie can be done when a buffer is no longer useful.  This is
 * just a wrapper around capping off the buffer at a
 * desired size zero.
 *
 * This operation does not fail.
 */
static inline void mem_buffer_delete (void *context, membuf *buffer) {
	mem_buffer_close (context, 0, buffer);
}


/** @brief Resize a dynamically-sized buffer to hold at least the desired size.
 *
 * Updates the buffer with a possibly updated bufptr and
 * buflen.  It is not an error to ask for space already
 * present in the buffer, just to ensure that enough
 * space is indeed available.  Your code should continue
 * with the new bufptr as a starting point, possibly by
 * always using `buffer->bufptr` as its start.
 *
 * You can use the entire buffer, even if it is larger
 * than your requested size, but you don't have to.  You
 * must not change the buflen in the buffer, however.
 *
 * This operation may have to reallocate the buffer, so
 * you should not have pointers into the buffer except
 * for offsets and integers.  When reallocating, the
 * old contents are moved into the beginning of the
 * new space.  Unlike the standard function realloc()
 * you can be sure that we will clear the old memory
 * before freeing it.
 *
 * Note that the work involved in these operations is
 * not cheap.
 *
 * Returns true on success (and updates buffer as described,
 * as well as zeroing any moved-from buffer) or false on failure
 * (when it leaves the existing buffer unchanged).
 */
bool mem_buffer_resize (void *context, mem_size_t min_size, membuf *buffer);


/** @def Test if a memory buffer is NULL.
 *
 * Test if the buffer is NULL, that is, the pointer is
 * set to NULL, irrespective of the length.
 */
#define mem_isnull(mb) ((mb)->bufptr == NULL)


/** @def Test if the buffer is empty.
 *
 * A buffer is considered empty when it has the length
 * set to 0, irrespective of the pointer.
 */
#define mem_isempty(mb) ((mb)->buflen == 0)
/** @} */


#endif

/** @} */
