/* This file is part of Quick MEM, version 0.1 and later.
 *     https://gitlab.com/arpa2/Quick-MEM
 *
 * Documentation on the actual memory layout is down below.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */

#include <assert.h>
#include <errno.h>
#include <stddef.h>
#include <string.h>


/* With DEBUG turned on, we want the ARPA2Common logging support;
 * otherwise we can ignore it, and just add the relevant no-debugging
 * version of log_debug() ourselves.
 */
#ifdef DEBUG
#include <arpa2/except.h>
#else
#define log_debug(fmt,...)
#endif

#include "arpa2/quick-mem.h"

/* A memory pool consists of two linked-lists:
 * - a linked list of (internal) pool blocks or fragments, held together
 *   through `struct mem_fragment`,
 * - a linked list of resources, held by `struct mem_resource_prefix`.
 *
 * A memory pool is associated with a context as follows: when creating
 * a memory pool, additional memory is allocated. A chunk of memory (a fragment)
 * is obtained (through `calloc()`) and then filled with a `struct mem_fragment`,
 * followed by a pointer to resources, followed by the actual context
 * data structure. The remainder of the block is free space.
 *
 * ```
 * +------+-----+------------+------------------------------------+
 * | frag | rp* | context... |        free space                  |
 * +------+-----+------------+------------------------------------+
 * ```
 *
 * The pointer to the context structure is returned. Functions that
 * take a context pointer can turn this into a pointer to the fragment
 * data by pointer arithmetic. See `context2pool()` for the
 * arithmetic definition. Getting from the context pointer to the
 * resource pointer is done with `context2res0()`.
 *
 * Additional fragments are allocated as needed and added to the
 * linked list of `mem_fragment` structures, through `mem_fragment.next`.
 *
 * ```
 * +------+-----+------------+------------------------------------+
 * | frag | rp* | context... |        free space                  |
 * +------+-----+------------+------------------------------------+
 *   |next
 *   v
 * +------+-------------------------------------------------------+
 * | frag |               free space                              |
 * +------+-------------------------------------------------------+
 * ```
 *
 * Fragments are **usually** MEM_FRAGMENTSIZE bytes, but unusually large
 * allocations can introduce larger fragments.
 *
 * The linked list of resources lives inside the fragments, so it
 * does not need to be freed separately. When a resource is added,
 * the linked-list structure is allocated within the pool (in some fragment)
 * followed immediately by the resource data, and then the list of resources
 * is updated.
 *
 * ```
 * +------+-----+------------+------+---------------+-------------+
 * | pool | rp* | context... | data | rp | resource | free space  |
 * +------+-----+------------+------+---------------+-------------+
 *           |                         ^
 *           +---next------------------+
 * ```
 *
 * In short: there is no separate "memory pool" data structure.
 * There is only a list of fragements, associated through pointer-
 * magic with a given context.
 */



/* Helper struct for the linked-list of resources present in
 * this pool; each resource has its own callback.
 */
struct mem_resource_prefix {
	void (*free_cb) (void *context, void *block);
	struct mem_resource_prefix *free_next;
};


/* The pointer to the first mem_resource_prefix is stored
 * in the first fragment of the pool, after the pool data and
 * just before the delivered context pointer.  We can derive
 * the first mem_resource_prefix from the context using
 * the following macro.  Do be careful to use the (void *)
 * and not the (void **) version of context!
 */
#define context2res0(context) (((struct mem_resource_prefix **) \
				 (context)) [-1])

/* The pool is a bundle of fragments that are combined
 * in, usually, MEM_FRAGMENTSIZE bytes.  Larger fragments may
 * be allocated separately (and will set a higher mem_size).
 * The actual pool is a linked list of mem_fragment structures.
 * The mem[] array holds the actual storage bytes, and mem_free
 * indicates the number of free memory bytes at the end.
 */
struct mem_fragment {
	struct mem_fragment *next;
	mem_size_t mem_size;
	mem_size_t mem_free;
	uint8_t mem [1];
};


/* The address of the first mem_fragment can be derived from
 * the context by subtracting two things: (1) the offset of mem
 * in the mem_fragment, and (2) the size of the initial part
 * in mem holding the pointer to the first mem_resource_prefix.
 * Do be careful to use the (void *) and not the (void **) version
 * of context (e.g. the pointer value, not the address of the pointer)!
 */
#define context2pool(context) ((struct mem_fragment *) \
		(((uint8_t *) (context)) \
			- offsetof (struct mem_fragment, mem) \
			- sizeof (struct mem_resource_prefix *)))

/* Elementary fragment size in bytes.  The size leaves some room for
 * the operating system to work on, but MEM_FREESIZE can be
 * optimised locally to get a perfectly OS-aligned mem_fragment.
 *
 * This is a mere implementation choice, taken into account only
 * when this file is compiled.  The value is not shown elsewhere,
 * nor does it restrict any other behaviour.
 */
#ifndef MEM_FRAGMENTSIZE
#define MEM_FRAGMENTSIZE 4000
#endif

/* Internal.  Allocate a block of bytes to the pool.
 * The old value of *out_pool must be NULL, use it to
 * link at the end of the pool list.
 */
static bool _mem_pool_add (size_t size, struct mem_fragment **out_pool) {
	assert (*out_pool == NULL);
	size_t allocsz = offsetof (struct mem_fragment, mem);
	if (size > MEM_FRAGMENTSIZE) {
		allocsz += size;
	} else {
		allocsz += MEM_FRAGMENTSIZE;
	}
	struct mem_fragment *newp = calloc (1, allocsz);
	log_debug ("Adding a pool segment of %zd bytes from %p", allocsz, newp);
	if (newp == NULL) {
		errno = ENOMEM;
		return false;
	}
	newp->mem_size = allocsz - offsetof (struct mem_fragment, mem);
	newp->mem_free = newp->mem_size;
	log_debug ("New pool segment %p has mem_size=%zd, mem_free=%zd", (void *) newp, newp->mem_size, newp->mem_free);
	*out_pool = newp;
	return true;
}


/* Internal.  Find a block with enough zeroed bytes in a pool.
 * The *pool may be NULL, in which case a new block will be
 * allocated.  The returned pointer is to the block that can
 * hold the required bytes, but those have not been allocated
 * yet.
 */
static struct mem_fragment *_mem_alloc (size_t size, struct mem_fragment **pool) {
	log_debug ("Allocating for %zd bytes", size);
	while (*pool != NULL) {
	log_debug ("Using *pool = %p; mem_free = %zd", *pool, (*pool)->mem_free);
		if ((*pool)->mem_free >= size) {
			return *pool;
		}
		pool = &(*pool)->next;
	}
	if (!_mem_pool_add (size, pool)) {
		/* errno is already set */
		return NULL;
	}
	return *pool;
}


/* Internal.  Free a complete pool but zero memory first.
 *
 * Up to keep_pools of the standard-sized pools will be kept.
 * Wildly sized ones are freed in any case, because their
 * reuse is unlikely and because they interfere with buffer
 * assumptions that larger sizes always have a block of their
 * own.
 */
static void _mem_zapfree (struct mem_fragment **pool, uint32_t keep_pools) {
	while (*pool) {
		struct mem_fragment *togo = *pool;
		/* Always clear the memory that is about to go */
		log_debug ("Zapping %zu bytes at %p clear", (size_t)togo->mem_size, togo->mem);
		memset (togo->mem, 0, togo->mem_size);
		if ((togo->mem_size == MEM_FRAGMENTSIZE) && (keep_pools > 0)) {
			/* Keep togo and point *pool at its next pointer */
			pool = &togo->next;
			keep_pools--;
			/* We still need to free the memory to recycle it */
			log_debug ("Recycling %zu bytes in a pool, upgrade from %zu", (size_t)togo->mem_size, (size_t)togo->mem_free);
			togo->mem_free = togo->mem_size;
		} else {
			/* Replace *pool with the next and free togo */
			*pool = togo->next;
			free (togo);
		}
	}
}

/* Internal function.
 *
 * Invoke all resource freeing functions in the pool.
 * This is done as part of mem_close() or mem_recycle().
 */
static void _mem_callback_resources (void *context) {
	log_debug ("context %p -> &res0 %p -> res0 %p", context, &context2res0 (context), context2res0 (context));
	struct mem_resource_prefix *res = context2res0 (context);
	while (res != NULL) {
		void *block = (void *) &res [1];
		log_debug ("res = %p", res);
		log_debug ("Freeing context %p resource %p with free_cb=%p", context, block, res->free_cb);
		res->free_cb (context, block);
		res = res->free_next;
	}
}


/*
 * We allocate space in the beginning of the first pool
 * for the head of the (struct mem_close_prefix *) list.
 *
 */
bool mem_open (mem_size_t context_size, void **context) {
	assert (*context == NULL);
	if (*context != NULL) {
		errno = EINVAL;
		return false;
	}
	struct mem_fragment *pool = NULL;
	context_size += sizeof (struct mem_resource_prefix *);
	if (!_mem_pool_add (context_size, &pool)) {
		return false;
	}
	log_debug ("New memory pool has mem_size=%zu to hold context_size=%zu", (size_t)pool->mem_size, (size_t)context_size);
	assert (pool->mem_size >= context_size);
	pool->mem_free = pool->mem_size - context_size;
	log_debug ("Address of newly added pool is %p, &pool->mem is %p and ->mem[+respfix] is %p", pool, &pool->mem, &pool->mem [sizeof (struct mem_resource_prefix *)]);
	*context = (void *) &pool->mem [sizeof (struct mem_resource_prefix *)];
	log_debug ("Returning context at %p in pool %p, with mem_size=%zu, mem_free=%zu", *context, pool, (size_t)pool->mem_size, (size_t)pool->mem_free);
	return true;
}


/*
 * The pool does not have to be linked explicitly from the
 * appdata; the appdata is simply the first allocated bit
 * in the data structure, and offset calculations suffice.
 *
 */
void mem_close (void **context) {
	if (*context == NULL) {
		return;
	}
	_mem_callback_resources (*context);
	struct mem_fragment *pool = context2pool (*context);
	log_debug ("context %p -> pool %p at %s:%d", *context, pool, __FILE__, __LINE__);
	_mem_zapfree (&pool, 0);
	*context = NULL;
}


/*
 * TODO: Keep initial structure?  Clear?  Size constraint?
 *
 * If the precondition fails, there's no way to indicate failure,
 * so don't bother with EINVAL either.
 */
void mem_recycle (mem_size_t keep_at_most, void *context) {
	assert (context != NULL);
	if (context == NULL) {
		return;
	}
	_mem_callback_resources (context);
	struct mem_fragment *pool = context2pool (context);
	assert (pool->mem_size == MEM_FRAGMENTSIZE);
	log_debug ("context %p -> pool %p at %s:%d", context, pool, __FILE__, __LINE__);
	_mem_zapfree (&pool, 1 + (keep_at_most / MEM_FRAGMENTSIZE));
	pool->mem_free -= sizeof (struct mem_resource_prefix *);
}


bool mem_alloc (void *context, mem_size_t block_size, void **block) {
	assert (context != NULL);
	assert (*block == NULL);
	if (context == NULL || *block != NULL) {
		errno = EINVAL;
		return false;
	}
	struct mem_fragment *pool = context2pool (context);
	log_debug ("context %p -> pool %p at %s:%d", context, pool, __FILE__, __LINE__);
	log_debug ("Allocating in context block %p", pool);
	pool = (void *) _mem_alloc (block_size, &pool);
	if (pool == NULL) {
		return false;
	}
	*block = pool->mem + pool->mem_size - pool->mem_free;
	pool->mem_free -= block_size;
	return true;
}


bool mem_resource (void *context, mem_size_t block_size, void **block,
			void (*free_cb) (void *context, void *block)) {
	assert (context != NULL);
	assert (*block == NULL);
	assert (free_cb != NULL);
	if (context == NULL || *block != NULL || free_cb == NULL) {
		errno = EINVAL;
		return false;
	}
	block_size += sizeof (struct mem_resource_prefix);
	struct mem_resource_prefix *block2 = NULL;
	if (!mem_alloc (context, block_size, (void **) &block2)) {
		return false;
	}
	block2->free_cb = free_cb;
	log_debug ("context %p -> &res0 %p -> res0 %p", context, &context2res0 (context), context2res0 (context));
	block2->free_next = context2res0 (context);
	context2res0 (context) = block2;
	*block = &block2 [1];
	return true;
}


bool mem_strdup (void *context, membuf str, char **out_strbuf) {
	assert (context != NULL);
	assert (*out_strbuf == NULL);
	if (context == NULL || *out_strbuf != NULL) {
		errno = EINVAL;
		return false;
	}
	if (!mem_alloc (context, str.buflen + 1, (void **) out_strbuf)) {
		/* errno is ENOMEM */
		return false;
	}
	log_debug ("Storing out_strbuf in %p", out_strbuf);
	log_debug ("Allocated %zd at %p, copying %zd to %p..%p and zeroing at %p", (long)(str.buflen+1), *out_strbuf, (long)(str.buflen), *out_strbuf, *out_strbuf + str.buflen-1, &(*out_strbuf) [str.buflen]);
	memcpy (*out_strbuf, str.bufptr, str.buflen);
	log_debug ("Allocated %zd at %p, copied  %zd to %p..%p and zeroing at %p", (long)(str.buflen+1), *out_strbuf, (long)(str.buflen), *out_strbuf, *out_strbuf + str.buflen-1, &(*out_strbuf) [str.buflen]);
	(*out_strbuf) [str.buflen] = '\0';  /* TODO: Already guaranteed */
	return true;
}


bool mem_buffer_open (void *context, mem_size_t min_size, membuf *buffer) {
	struct mem_fragment *pool = context2pool (context);
	log_debug ("context %p -> pool %p at %s:%d", context, pool, __FILE__, __LINE__);
	pool = _mem_alloc (min_size, &pool);
	if (pool == NULL) {
		return false;
	}
	buffer->bufptr = pool->mem + pool->mem_size - pool->mem_free;
	buffer->buflen = pool->mem_free;
	pool->mem_free = 0;
	log_debug ("mem_buffer_open() on pool %p", pool);
	log_debug ("Opened buffer %p is at %p sized %zd", buffer, buffer->bufptr, buffer->buflen);
	return true;
}


void mem_buffer_close (void *context, mem_size_t final_size, membuf *buffer) {
	log_debug ("Closing buffer %p is at %p sized %zd", buffer, buffer->bufptr, buffer->buflen);
	uint8_t *mem0 = buffer->bufptr;
	mem_size_t memsz = buffer->buflen;
	assert (final_size <= memsz);
	if (memsz < MEM_FRAGMENTSIZE) {
		mem0 -= MEM_FRAGMENTSIZE - memsz;
		memsz = MEM_FRAGMENTSIZE;
	}
	struct mem_fragment *bufpool = (void *) (
		((uint8_t *) mem0)
			- offsetof (struct mem_fragment, mem));
	log_debug ("mem_buffer_close() to %zd on pool %p", final_size, bufpool);
	log_debug ("mem_buffer_close() expects mem_free=0, found %zd", (long)(bufpool->mem_free));
	assert (bufpool->mem_free == 0);
	assert (bufpool->mem_size == memsz);
	bufpool->mem_free = buffer->buflen - final_size;
	/* The buffer may have been written, so clear it */
	memset (buffer->bufptr + final_size, 0, bufpool->mem_free);
	buffer->buflen = final_size;
}


bool mem_buffer_resize (void *context, mem_size_t min_size, membuf *buffer) {
	log_debug ("Resizing buffer %p is at %p sized %zd min_size %zd", buffer, buffer->bufptr, (long)(buffer->buflen), (long)(min_size));
	if (min_size <= buffer->buflen) {
		return true;
	}
	/* Allocate with MEM_FRAGMENTSIZE bytes extra.
	 * This may or may not be optimal, it is not
	 * assumed elsewhere in the code.  The idea
	 * is that the extra memory will be released
	 * when the buffer is closed, so it should
	 * not be detrimental; at the same time it
	 * can reduce the number of actual resizes.
	 */
	if (min_size < MEM_FRAGMENTSIZE / 2) {
		/* This should lead to a reusable pool */
		min_size = MEM_FRAGMENTSIZE;
	} else {
		min_size = min_size + MEM_FRAGMENTSIZE;
	}
	struct mem_fragment *pool = context2pool (context);
	log_debug ("context %p -> pool %p at %s:%d", context, pool, __FILE__, __LINE__);
	pool = _mem_alloc (min_size, &pool);
	log_debug ("mem_buffer_resize() on new pool %p", pool);
	assert (pool->mem_size >= min_size);
	if (pool == NULL) {
		return false;
	}
	/* Move the old content over this new */
	memcpy (pool->mem, buffer->bufptr, buffer->buflen);
	/* Free the old content (and have the memory zeroed) */
	log_debug ("Freeing old buffer at context %p", context);
	mem_buffer_close (context, 0, buffer);
	/* Return the result, which is the complete new pool */
	pool->mem_free = 0;
	buffer->bufptr = pool->mem;
	buffer->buflen = pool->mem_size;
	log_debug ("Resized buffer %p is at %p sized %zd", buffer, buffer->bufptr, buffer->buflen);
	return true;
}
