<!-- SPDX-FileCopyrightText: Copyright 2020, Rick van Rein, OpenFortress.nl
     SPDX-License-Identifier: BSD-2-Clause
-->

# Quick MEM

> Quick 'n Easy Memory Pool for ARPA2 projects

## v1.0.0 (2025-01-08)

- Released long-standing code as v1.0.0

## v0.3.1 (2022-10-31)

- Packaging improvements
- Use ARPA2CM 1.0
- No functional changes

## v0.3.0 (2022-02-10)

- Documentation fixes
- Relax requirement for ARPA2Common (only for DEBUG builds)
- Relax requirement for Doxygen  (only for docs)

## v0.2.2 (2021-07-15)

- Cleanups for release, chasing dependencies

## v0.2.1 (2021-06-02)

- Repaired bug in allocation of new pool segments

## v0.2.0 (2021-01-11)

- ARPA2Common is needed for logging
- Add docs target

## v0.1.0 (2020-11-26)

- First release of split-off-from-KIP library

## Pre-0.1 Migration from KIP to Quick-MEM

> *Origin code: KIP branch initial at 50cc17a169e3617358b8dfaa2c52539e9e25c141*

### Changes from KIP

 - Taken out Quick-DER support (`_pack`, `_unpack`, `_send`, `_recv`)
 - Move that into Quick-DER package as a Quick-DERMEM export
 - Change `DERMEM_` and `dermem_` prefixes to `DER_` and `der_`
 - Rename `dercursor` types to `membuf` types
 - Include paths and libraries are found as `Quick-MEM`
 - Include file is `<arpa2/quick-mem.h>`
 - Rename `derptr` and `derlen` to `ptr` and `len`

### To use with Quick-DER

 - This package is an indirect dependency
 - Build Quick-DER instead; do not suppress its memory pool support
 - Include paths and libraries are found as `Quick-DERMEM`
 - Include file is `<arpa2/quick-dermem.h>`
 - Distinguish type `membuf{.ptr,.len}` from `dercursor{.derptr,.derlen}`
 - Wrappers `dermem_` around `der_`  cast types `membuf` to `dercursor`

