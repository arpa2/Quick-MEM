<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Memory Pooling for ARPA2 projects

> *This package Quick MEM is useful for dealing with
> memory pools, which reduce memory fragmentation by
> allocating (smaller chunks of) memory from pools that are
> destined for destruction all at the same time.*

When processing packets, or chunks of data, or LDAP messages,
or any of a host of other data forms within the ARPA2 project,
a pattern emerges: during processing of one "thing", multiple
memory allocations are needed, while at the end all of those
allocations can be released at once.

To reduce overall memory fragmentation, and to reduce the amount
of work the system's memory allocator needs to do, a
memory pool is introduced: a larger chunk of memory that
is allocated (once) and released (once) from the system memory,
but for the purpose of an application acts as an independent
allocate-only source of memory.

It can be particularly useful when combined with
[Quick-DER](https://gitlab.com/arpa2/quick-der),
which processes highly dynamic structures. Quick-DER
builds upon the memory pools from Quick MEM.


## Using Quick MEM

To use the `mem_` functions, there is an include file

```
#include <arpa2/quick-mem.h>
```

and a library, called `quickmem`.
To link with Quick MEM, use `-lquickmem`, or in a CMake context,
use `ARPA2::Quick-MEM`.
That is all you need.  The software is small but potent.

The functions are extensively documented in the
include file.


### Minimal Example

An application typically has some "context" data for a
given operation. Quick MEM associates a pool with a context.
(If a application context has no relevant data, a zero-sized
"context" is fine). A pool is created with `mem_open()` and
finally released with `mem_close()`. Memory is allocated from
the pool with `mem_alloc()`, which does not need to be separately
released: it goes away with the pool.

```
    typedef struct { ... } context;
    typedef struct { ... } data;
    context *operation_context = NULL;
    if (mem_open(sizeof(context), &operation_context)) {
        int *my_int = NULL;
        data *my_data = NULL;
        mem_alloc(context, sizeof(int), &my_int);
        mem_alloc(context, sizeof(data), &my_data);
        ...
    }
    mem_close(&operation_context);
```

There is no need to reserve space for memory pool data in the
context structure. This is attached invisibly during the allocation
of the memory for the context structure itself. The context pointer
(even for zero-sized "context"s) is passed to Quick MEM operations.


## Pooling memory

The idea behind the `mem_` functions is that memory
management is greatly simplified when many fragments are
taken from a pool, which is freed all at once.  This can
help to improve referential integrity and reduce the risk
of memory leaks, but if the latter does not happen to you
either, you will at least appreciate the simplification
for your code: no need to concientiously clean up the
right data at the various stages of failure during a
somewhat complex function that should work atomically.

The idea is that most software has a "context" structure
that might host a memory pool. Using the he `mem_` API
starts off  by adding a pool to the "context" structure.
Then, whenever you need to allocate memory, you take
it out from the pool associated with the "context",
and when you cleanup the context, you are done.

It is quite possible to have many memory pools at the
same time.  At the time you allocate memory, you just
pick the one that is cleaned up at the right point.
So you might end up with a memory pool for a network
connection, another for the current request, and so on.

The pool is in fact a list of memory blocks of a fixed
size (current default is 4000 bytes of user memory) into
which any number of smaller blocks may be lined up.
Blocks up to the default size are easy, for the larger
ones a customary call to `malloc()` is made but without
the need to manage the accompanying `free()` too, because
these larger blocks are also added to the list.


## Recycling Memory Pools

A memory pool is usually cleaned up at the end of something,
like the closing of a connection or having finished a
request or a protocol phase.  Many times however, a
new phase will be launched immediately, for instance
another request will be anticipated.  In such cases,
closing and openening a memory pool may not be as
efficient as recycling the memory pool.  This is
especially true if the work may be expected to have
similar memory requirements.

To help with this, there is an option of clearing a
memory pool and thereby recycle its storage.  When
used effectively, a service should have very little
need to call the operating system's memory allocation
anymore.

Memory pools contain mostly chunks of a standard size,
and possibly a few extra ones of larger size.  These
latter ones are far less likely to be reusable, and
may lead to memory fragmentation.  To avoid that in
long-running protocols, such non-standard chunks are
freed while clearing the memory pool, and only the
standard-sized chunks will recycle.


## Buffers for Dynamic Sizing

The pool can open a buffer whose final size is not yet
known at the time of allocation.  Internally, this
reserves the remainder of a memory block on the pool.
When the buffer is closed, its final size is given
and the remainder after it returns to the pool.  This
is very efficient for operations such as string buffer
concatenation or `SEQUENCE OF` collection for Quick DER.

It is not a problem to continue to do other memory
operations while a buffer is open; it will just be
more likely to allocate a new chunk.  Multiple buffers
can co-exist in the same fashion.

Buffers may even be grown beyond the initial minimum
size requested.  If this happens, the operation is
much like standard `realloc()` which allocates a new
buffer at another address.  We allocate more than
requested to make it easy to expand gradually, and
still the logic of closing the buffer to return the
unused poriton to the memory pool works.

Buffers are allocated as a `membuf` structure.
Its `.bufptr` points to the memory in use; if the
buffer may be dynamically grown, then this pointer
will be changed.  The `.buflen` indicates the
buffer space, which you may choose to use or not,
but you must not replace the value in this
`buflen`, as it is used for buffer management.
Once you close a buffer, you should consider the
remaining space like any normal allocation, but
no longer as a buffer.


## Zeroing and Resource Cleanup

The memory is always set to zero bytes when it is
allocated.  When it is cleaned up or recycled, it is also set to zero.
This is useful in security systems, where keys may be
stored in a memory pool.  It is generally interesting
from a privacy perspective.  Without this, another
procedure might call plain `malloc()` and receive the
memory pre-filled with your old data.

Instead of allocating memory, you can also create it
as a resource.  This is also memory, but with an
attached cleanup routine.  This routine will be called
when the memory pool is freed, but before zeroing the
memory.  You can use it to free resources such as
files.

> TODO: Only the pool handle is not a resource; not sure if this is useful.

Resources are cleaned up in the opposite order that
they were created, in the hope to arrange them
properly in terms of dependencies.

These mechanisms also work as expected in the more
complex cases.  Closing or resizing of buffers does
not cause any old contents to be visible (which is
a good improvement over `realloc()` in any case).
And when recycling a memory pool, resources will be
cleaned up and the memory will be cleared to zero,
regardless of it being reused or returned to the
operating system pool.


## Relation with Valgrind

Memory testers like `valgrind` will find a number of
problems, like:

  * Leaking or not freeing memory
  * Freeing memory more than once
  * Accessing uninitialised memory
  * Accessing freed or unallocated memory

The first three are mostly avoided with Quick MEM.
Mostly, because you might leak a complete memory
pool, but not the individual fragments in each.

Access to unallocated memory is very often a
write just before or just after an allocated
region, which under a memory pool is usually
just another region owned by the same program.
You will lose this valuable information if
you use Quick MEM (or most other memory pools).

In practice, it turns out that memory pools
show conceptual errors when memory is accessed
from a pool that was already considered closed.
Either because the memory is deallocated, or
because it was cleared.  This also helps with
memory regions attached to a context handle.
So, in effect, the memory pools actually support
debugging.  One factor that makes this less
useful is when memory is recycled, which causes
memory blocks to be retained.  This can easily
be circumvented by not efficiently recycling
if `DEBUG` is defined, but instead go through
the tedious combination of `mem_close()`
and `mem_open()` instead.  Or, better even,
doing it in the opposite order to completely
disable the chance of recycling the memory
through the operating system.  It is however
best if the application makes this switch, so
as to minimise the impact of `DEBUG` on the
operational reality and allow the programmer
maximum control during debugging.


## Possible Extensions

Considered, but not yet implemented:

  * Always have larger blocks at the end of the pool
  * WONTFIX: Valgrind test mode: using plain `malloc()` and `free()`
